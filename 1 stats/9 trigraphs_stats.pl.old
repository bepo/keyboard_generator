#!/usr/bin/perl -w

use strict;

my $stored_trigraphs_nb = 0; # Number of stored trigraphs

my %symbols  = (); # Hash table of stored symbols
my %trigraphs = (); # Hash table of stored trigraphs

my $key_number = 48; # Number of key we want tu map on keyboard

#-------------------------------------------------------------------------------

die("Usage: $0 <corpus file> <symbols statistics file> <trigraph statistics file>\n")
    if (!defined($ARGV[2]));

die("File $ARGV[0] doesn't exist\n")
    if (! -e $ARGV[0]);

die("File $ARGV[1] doesn't exist\n")
    if (! -e $ARGV[1]);

my $CORPUS_FILE   = $ARGV[0];
my $SYMBOL_FILE   = $ARGV[1];
my $TRIGRAPH_FILE = $ARGV[2];

#-------------------------------------------------------------------------------

# Get both stress and letter from a stressed letter
sub get_stress_letter ($)
{
    my $symbol = shift;
    my ($stress, $letter);

    # Getting stress
    if    ($symbol =~ /[�����]/)   { $stress = "dead-`"; }
    elsif ($symbol =~ /[�������]/) { $stress = "dead-�"; }
    elsif ($symbol =~ /[�����]/)   { $stress = "dead-^"; }
    elsif ($symbol =~ /[�������]/) { $stress = "dead-�"; }
    elsif ($symbol =~ /[���]/)     { $stress = "dead-~"; }
    elsif ($symbol =~ /[�]/)       { $stress = "dead-�"; }

    # Getting letter
    if    ($symbol =~ /[������]/)  { $letter = "a"; }
    elsif ($symbol =~ /[����]/)    { $letter = "e"; }
    elsif ($symbol =~ /[����]/)    { $letter = "i"; }
    elsif ($symbol =~ /[�����]/)   { $letter = "o"; }
    elsif ($symbol =~ /[����]/)    { $letter = "u"; }
    elsif ($symbol =~ /[��]/)      { $letter = "y"; }
    elsif ($symbol =~ /[�]/)       { $letter = "n"; }
    elsif ($symbol =~ /[�]/)       { $letter = "dead-�"; }
    elsif ($symbol =~ /[�]/)       { $letter = "dead-�"; }

    return ($stress, $letter);
}

#-------------------------------------------------------------------------------

# Loading symbols stats

open(FILE, "< $SYMBOL_FILE") or die("open: $!");

while (defined(my $line = <FILE>))
{
    if ($line =~ /^(.+)\t.+\t(.+)$/)
    {
        my $symbol      = $1;
        my $instance_nb = $2;

        $symbols{$symbol} = $instance_nb;
    }
}

close(FILE);

#-------------------------------------------------------------------------------

# Assuming there is one key for each symbol

my $symbol_number = $key_number;


# Sorting symbols by instance number (decreasing)

my @sorted_symbols;
my $i = 0;
my $skipped_digit = 0;

foreach my $key (sort{$symbols{$b} <=> $symbols{$a}} (keys(%symbols)))
{
    $sorted_symbols[$i] = $key;

    $i++;
}

# Getting less frequent symbol being mapped on keymap with direct access 

my $last_symbol = $sorted_symbols[$symbol_number - 1];

#-------------------------------------------------------------------------------

# Getting trigraph statistics

my $prev_symbol_1;
my $prev_symbol_2;

open(FILE, "< $CORPUS_FILE") or die("open: $!");

while (defined(my $line = <FILE>))
{
    for (my $i = 0; $i < length($line); $i++)
    {
        my $symbol = substr($line, $i, 1);

        if (defined($symbols{$symbol}))
            # Known symbol (direct access)
        {
            if (defined($prev_symbol_1) and ($symbol ne $prev_symbol_1))
            {
                if (defined($prev_symbol_2))
                {
                    # No need of second symbol, just storing fisrt and third ones
                    my $trigraph = $prev_symbol_2.$symbol;

                    if (exists($trigraphs{$trigraph})) { $trigraphs{$trigraph}++; }
                    else { $trigraphs{$trigraph} = 1; }

                    $stored_trigraphs_nb++;
                }
            }
            $prev_symbol_2 = $prev_symbol_1;
            $prev_symbol_1 = $symbol;
        }
        elsif ($symbol =~ /[����������������������������]/)
            # Stressed letter
        {
            my ($stress, $letter) = get_stress_letter($symbol);

            # Must repeat operation for letter and for stress symbol

            if (defined($symbols{$stress}))
                # Known stress (direct access)
            {
                if (defined($prev_symbol_1) and ($stress ne $prev_symbol_1))
                {
                    if (defined($prev_symbol_2))
                    {
                        # No need of second symbol, just storing fisrt and third ones
                        my $trigraph = $prev_symbol_2.$stress;

                        if (exists($trigraphs{$trigraph})) { $trigraphs{$trigraph}++; }
                        else { $trigraphs{$trigraph} = 1; }

                        $stored_trigraphs_nb++;
                    }
                }
                $prev_symbol_2 = $prev_symbol_1;
                $prev_symbol_1 = $stress;
            }
            else
                # No direct access to stress
            {
                # Breaking trigraph
                undef $prev_symbol_1;
                undef $prev_symbol_2;
            }

            if (defined($symbols{$letter}))
                # Known letter (direct access)
            {
                if (defined($prev_symbol_1) and ($letter ne $prev_symbol_1))
                {
                    if (defined($prev_symbol_2))
                    {
                        # No need of second symbol, just storing fisrt and third ones
                        my $trigraph = $prev_symbol_2.$letter;

                        if (exists($trigraphs{$trigraph})) { $trigraphs{$trigraph}++; }
                        else { $trigraphs{$trigraph} = 1; }

                        $stored_trigraphs_nb++;
                    }
                }
                $prev_symbol_2 = $prev_symbol_1;
                $prev_symbol_1 = $letter;
            }
            else
                # No direct access to letter
            {
                # Breaking trigraph
                undef $prev_symbol_1;
                undef $prev_symbol_2;
            }
        }
        else
            # Unknown symbol
        {
            # Breaking trigraph
            undef $prev_symbol_1;
            undef $prev_symbol_2;
        }
    }
}

close(FILE);

#-------------------------------------------------------------------------------

# Display trigraph list with instance number

open(FILE, "> $TRIGRAPH_FILE") or die("open: $!");

$i = 1;

foreach my $key (sort{$trigraphs{$b} <=> $trigraphs{$a}} (keys(%trigraphs)))
{
    my $percent = sprintf("%.3f", 100 * $trigraphs{$key} / $stored_trigraphs_nb);

    print FILE "$key\t$i\t$trigraphs{$key}\t$percent\n";

    $i++;
}

close(FILE);
