#!/usr/bin/perl -w

#  This file is part of KeyboardGenerator.
#
#  KeyboardGenerator is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  KeyboardGenerator is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with KeyboardGenerator.  If not, see <http://www.gnu.org/licenses/>.

use strict;

my %symbols = (); # Hash table of stored symbols

#-------------------------------------------------------------------------------

die("Usage: $0 <corpus file> <input symbols statistics file> <output symbols statistics file>\n")
    if (!defined($ARGV[2]));

die("File $ARGV[0] doesn't exist\n")
    if (! -e $ARGV[0]);

die("File $ARGV[1] doesn't exist\n")
    if (! -e $ARGV[1]);

my $CORPUS_FILE        = $ARGV[0];
my $INPUT_SYMBOL_FILE  = $ARGV[1];
my $OUTPUT_SYMBOL_FILE = $ARGV[2];

#-------------------------------------------------------------------------------

# Parse corpus file and calculate count of "^^"

open(FILE, "< $CORPUS_FILE") or die("open: $!");

my $count = 0;

while (defined(my $line = <FILE>))
{
    $count += ($line =~ s/\^\^//g);
}

close(FILE);

#-------------------------------------------------------------------------------

# Load symbols stats

open(FILE, "< $INPUT_SYMBOL_FILE") or die("open: $!");

while (defined(my $line = <FILE>))
{
    if ($line =~ /^(.+)\t.+\t(.+)$/)
    {
        my $symbol      = $1;
        my $instance_nb = $2;

        $symbols{$symbol} = $instance_nb;
    }
}

close(FILE);

#-------------------------------------------------------------------------------

# Reduce stats of ^ of twice the count of ^^ and create stats of dead-^

$symbols{"^"}     -= 2 * $count;
$symbols{"dead-^"} = 2 * $count;

#-------------------------------------------------------------------------------

# Store symbols statistics

open(FILE, "> $OUTPUT_SYMBOL_FILE") or die("open: $!");

my $i = 1;

foreach my $symbol (sort{$symbols{$b} <=> $symbols{$a}} (keys(%symbols)))
{
    print FILE "$symbol\t$i\t$symbols{$symbol}\n";

    $i++;
}

close(FILE);
