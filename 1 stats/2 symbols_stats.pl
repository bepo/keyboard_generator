#!/usr/bin/perl -w

#  This file is part of KeyboardGenerator.
#
#  KeyboardGenerator is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  KeyboardGenerator is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with KeyboardGenerator.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use locale;
# To have lowercase working on stressed letter, ie lc('�') returning �, not �.

my %symbols = (); # Hash table of stored symbols

#-------------------------------------------------------------------------------

die("Usage: $0 <corpus file> <symbols statistics file>\n")
    if (!defined($ARGV[1]));

die("File $ARGV[0] doesn't exist\n")
    if (! -e $ARGV[0]);

my $CORPUS_FILE = $ARGV[0];
my $SYMBOL_FILE = $ARGV[1];

#-------------------------------------------------------------------------------

# Get lowercase symbol of a symbol
# Will be used to define which symbol will mapped on same key
# 'A' will be mapped on same key than 'a'
# '�' will be mapped on same key than '�'
# '/' will be mapped on same key than ':'
#
# Also define :
# - lowercase for stressed letters,
# - lowercase for punctuation.
sub get_lowercase ($)
{
    my $symbol = shift;

    my $lc_symbol = lc($symbol);

# Old
#    if    ($symbol eq '/') { $lc_symbol = ':'; }
#    elsif ($symbol eq '?') { $lc_symbol = ','; }
#    elsif ($symbol eq '!') { $lc_symbol = '.'; }

    if    ($symbol eq '/') { $lc_symbol = ':'; }
    elsif ($symbol eq '?') { $lc_symbol = ','; }
    elsif ($symbol eq ';') { $lc_symbol = '.'; }
    elsif ($symbol eq '!') { $lc_symbol = "'"; }

    return $lc_symbol;
}

#-------------------------------------------------------------------------------

# Getting symbols statistics (without taking care of case)

open(FILE, "< $CORPUS_FILE") or die("open: $!");

while (defined(my $line = <FILE>))
{
    for (my $i = 0; $i < length($line); $i++)
    {
        my $symbol = substr($line, $i, 1);
        $symbol = &get_lowercase($symbol);

        if ($symbol !~ /[\t\r\n\s\xA0]/)
            # Don't take care of those symbols
            # \xA0 is the no-break space character
        {
            if (exists($symbols{$symbol})) { $symbols{$symbol}++; }
            else                           { $symbols{$symbol} = 1; }
        }
    }
}

close(FILE);

#-------------------------------------------------------------------------------

# Store symbols statistics

open(FILE, "> $SYMBOL_FILE") or die("open: $!");

my $i = 1;

foreach my $symbol (sort{$symbols{$b} <=> $symbols{$a}} (keys(%symbols)))
{
    print FILE "$symbol\t$i\t$symbols{$symbol}\n";

    $i++;
}

close(FILE);
