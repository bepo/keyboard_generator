#!/usr/bin/perl -w

#  This file is part of KeyboardGenerator.
#
#  KeyboardGenerator is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  KeyboardGenerator is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with KeyboardGenerator.  If not, see <http://www.gnu.org/licenses/>.

use strict;

my %symbols = (); # Hash table of stored symbols

my $key_number = 48; # Number of key we want tu map on keyboard

#-------------------------------------------------------------------------------

die("Usage: $0 <input symbols statistics file> <output symbols statistics file>\n")
    if (!defined($ARGV[1]));

die("File $ARGV[0] doesn't exist\n")
    if (! -e $ARGV[0]);

my $INPUT_SYMBOL_FILE  = $ARGV[0];
my $OUTPUT_SYMBOL_FILE = $ARGV[1];

#-------------------------------------------------------------------------------

# Get both stress and letter from a stressed letter
sub get_stress_letter ($)
{
    my $symbol = shift;
    my ($stress, $letter);

    # Getting stress
    if    ($symbol =~ /[�����]/)   { $stress = "dead-`"; }
    elsif ($symbol =~ /[�������]/) { $stress = "dead-�"; }
    elsif ($symbol =~ /[�����]/)   { $stress = "dead-^"; }
    elsif ($symbol =~ /[�������]/) { $stress = "dead-�"; }
    elsif ($symbol =~ /[���]/)     { $stress = "dead-~"; }
    elsif ($symbol =~ /[�]/)       { $stress = "dead-�"; }

    # Getting letter
    if    ($symbol =~ /[������]/)  { $letter = "a"; }
    elsif ($symbol =~ /[����]/)    { $letter = "e"; }
    elsif ($symbol =~ /[����]/)    { $letter = "i"; }
    elsif ($symbol =~ /[�����]/)   { $letter = "o"; }
    elsif ($symbol =~ /[����]/)    { $letter = "u"; }
    elsif ($symbol =~ /[��]/)      { $letter = "y"; }
    elsif ($symbol =~ /[�]/)       { $letter = "n"; }
    elsif ($symbol =~ /[�]/)       { $letter = "dead-�"; }
    elsif ($symbol =~ /[�]/)       { $letter = "dead-�"; }

    return ($stress, $letter);
}

#-------------------------------------------------------------------------------

# Loading symbols stats

open(FILE, "< $INPUT_SYMBOL_FILE") or die("open: $!");

while (defined(my $line = <FILE>))
{
    if ($line =~ /^(.+)\t.+\t(.+)$/)
    {
        my $symbol      = $1;
        my $instance_nb = $2;

        $symbols{$symbol} = $instance_nb;
    }
}

close(FILE);

#-------------------------------------------------------------------------------

# Computing number of different rated symbols

my @symbols_array = %symbols;
my $different_symbol_nb = ($#symbols_array + 1) / 2;

# Sorting symbols by instance number (decreasing)

my @sorted_symbols;
my $i = 0;
my $skipped_digit = 0;

foreach my $key (sort{$symbols{$b} <=> $symbols{$a}} (keys(%symbols)))
{
    # Assuming digit are not mapped with direct access, but while using shift key

    if ($key !~ /[0-9]/)
    {
        $sorted_symbols[$i] = $key;
        $i++;
    }
    else
    {
        $skipped_digit++;
    }
}

# Getting less frequent symbol being mapped on keymap with direct access 

my $last_symbol = $sorted_symbols[$key_number - 1];
my $previous_last_symbol;
# Replacing each stressed letters not being mapped directly by letter and stress
# Repeat replace operation until no new change happen

do
{
    for (my $i = $key_number; $i < $different_symbol_nb - $skipped_digit; $i++)
    {
        my $symbol = $sorted_symbols[$i];

        if ($symbol =~ /[����������������������������]/)
            # Stressed letter
        {
            # Getting letter frequency
            my $frequency = $symbols{$symbol};
            my ($stress, $letter) = get_stress_letter($symbol);

            # Increase instance number of stress
            if (exists($symbols{$stress})) { $symbols{$stress} += $frequency; }
            else { $symbols{$stress} = 1; }

            # Increase instance number of letter
            if (exists($symbols{$letter})) { $symbols{$letter} += $frequency; }
            else { $symbols{$letter} = 1; }

            # Removing stress letter
            delete $symbols{$symbol};
        }
    }


    # Remember previous last symbol

    $previous_last_symbol = $last_symbol;


    # Computing number of different rated symbols

    @symbols_array = %symbols;
    $different_symbol_nb = ($#symbols_array + 1) / 2;


    # Sorting symbols by instance number (decreasing)

    $i = 0;
    $skipped_digit = 0;

    foreach my $key (sort{$symbols{$b} <=> $symbols{$a}} (keys(%symbols)))
    {
        if ($key !~ /[0-9]/)
        {
            $sorted_symbols[$i] = $key;
            $i++;
        }
        else
        {
            $skipped_digit++;
        }
    }


    # Getting new last symbol beeing mapped
    $last_symbol = $sorted_symbols[$key_number - 1];
}
while ($last_symbol ne $previous_last_symbol);

#-------------------------------------------------------------------------------

# Store symbols statistics

open(FILE, "> $OUTPUT_SYMBOL_FILE") or die("open: $!");

$i = 1;

foreach my $symbol (sort{$symbols{$b} <=> $symbols{$a}} (keys(%symbols)))
{
    print FILE "$symbol\t$i\t$symbols{$symbol}\n";

    $i++;
}

close(FILE);
