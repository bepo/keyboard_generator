#!/usr/bin/perl

#  This file is part of KeyboardGenerator.
#
#  KeyboardGenerator is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  KeyboardGenerator is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with KeyboardGenerator.  If not, see <http://www.gnu.org/licenses/>.

use strict;

my %symbols = (); # Hash table of stored symbols

#-------------------------------------------------------------------------------

die("Usage: $0 <input symbols statistics file> <output symbols statistics file>\n")
    if (!defined($ARGV[1]));

die("File $ARGV[0] doesn't exist\n")
    if (! -e $ARGV[0]);

my $INPUT_SYMBOL_FILE  = $ARGV[0];
my $OUTPUT_SYMBOL_FILE = $ARGV[1];

#-------------------------------------------------------------------------------

# Loading symbols stats

open(FILE, "< $INPUT_SYMBOL_FILE") or die("open: $!");

while (defined(my $line = <FILE>))
{
    if ($line =~ /^(.+)\t.+\t(.+)$/)
    {
        my $symbol      = $1;
        my $instance_nb = $2;

        $symbols{$symbol} = $instance_nb;
    }
}

close(FILE);

#-------------------------------------------------------------------------------

# Creating dead-circum and dead-diaresis stats

$symbols{"dead-^"} = 0;
$symbols{"dead-�"} = 0;

$symbols{"dead-^"} += $symbols{"�"};
$symbols{"dead-^"} += $symbols{"�"};
$symbols{"dead-^"} += $symbols{"�"};
$symbols{"dead-^"} += $symbols{"�"};
$symbols{"dead-^"} += $symbols{"�"};

$symbols{"dead-�"} += $symbols{"�"};
$symbols{"dead-�"} += $symbols{"�"};
$symbols{"dead-�"} += $symbols{"�"};
$symbols{"dead-�"} += $symbols{"�"};
$symbols{"dead-�"} += $symbols{"�"};
$symbols{"dead-�"} += $symbols{"�"};

# Changing letters stats

$symbols{"a"} += $symbols{"�"};
$symbols{"e"} += $symbols{"�"};
$symbols{"i"} += $symbols{"�"};
$symbols{"o"} += $symbols{"�"};
$symbols{"u"} += $symbols{"�"};

$symbols{"a"} += $symbols{"�"};
$symbols{"e"} += $symbols{"�"};
$symbols{"i"} += $symbols{"�"};
$symbols{"o"} += $symbols{"�"};
$symbols{"u"} += $symbols{"�"};
$symbols{"y"} += $symbols{"�"};

# Removing stressed letters stats

delete $symbols{"�"};
delete $symbols{"�"};
delete $symbols{"�"};
delete $symbols{"�"};
delete $symbols{"�"};

delete $symbols{"�"};
delete $symbols{"�"};
delete $symbols{"�"};
delete $symbols{"�"};
delete $symbols{"�"};
delete $symbols{"�"};

#-------------------------------------------------------------------------------

# Store symbols statistics

open(FILE, "> $OUTPUT_SYMBOL_FILE") or die("open: $!");

my $i = 1;

foreach my $symbol (sort{$symbols{$b} <=> $symbols{$a}} (keys(%symbols)))
{
    print FILE "$symbol\t$i\t$symbols{$symbol}\n";

    $i++;
}

close(FILE);
