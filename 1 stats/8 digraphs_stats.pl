#!/usr/bin/perl -w

#  This file is part of KeyboardGenerator.
#
#  KeyboardGenerator is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  KeyboardGenerator is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with KeyboardGenerator.  If not, see <http://www.gnu.org/licenses/>.

use strict;

my $stored_digraphs_nb = 0; # Number of stored digraphs

my %symbols  = (); # Hash table of stored symbols
my %digraphs = (); # Hash table of stored digraphs

my $key_number = 48; # Number of key we want tu map on keyboard

#-------------------------------------------------------------------------------

die("Usage: $0 <corpus file> <symbols statistics file> <digraph statistics file>\n")
    if (!defined($ARGV[2]));

die("File $ARGV[0] doesn't exist\n")
    if (! -e $ARGV[0]);

die("File $ARGV[1] doesn't exist\n")
    if (! -e $ARGV[1]);

my $CORPUS_FILE  = $ARGV[0];
my $SYMBOL_FILE  = $ARGV[1];
my $DIGRAPH_FILE = $ARGV[2];

#-------------------------------------------------------------------------------

# Get both stress and letter from a stressed letter
sub get_stress_letter ($)
{
    my $symbol = shift;
    my ($stress, $letter);

    # Getting stress
    if    ($symbol =~ /[�����]/)   { $stress = "dead-`"; }
    elsif ($symbol =~ /[�������]/) { $stress = "dead-�"; }
    elsif ($symbol =~ /[�����]/)   { $stress = "dead-^"; }
    elsif ($symbol =~ /[�������]/) { $stress = "dead-�"; }
    elsif ($symbol =~ /[���]/)     { $stress = "dead-~"; }
    elsif ($symbol =~ /[�]/)       { $stress = "dead-�"; }

    # Getting letter
    if    ($symbol =~ /[������]/)  { $letter = "a"; }
    elsif ($symbol =~ /[����]/)    { $letter = "e"; }
    elsif ($symbol =~ /[����]/)    { $letter = "i"; }
    elsif ($symbol =~ /[�����]/)   { $letter = "o"; }
    elsif ($symbol =~ /[����]/)    { $letter = "u"; }
    elsif ($symbol =~ /[��]/)      { $letter = "y"; }
    elsif ($symbol =~ /[�]/)       { $letter = "n"; }
    elsif ($symbol =~ /[�]/)       { $letter = "dead-�"; }
    elsif ($symbol =~ /[�]/)       { $letter = "dead-�"; }

    return ($stress, $letter);
}

#-------------------------------------------------------------------------------

# Loading symbols stats

open(FILE, "< $SYMBOL_FILE") or die("open: $!");

while (defined(my $line = <FILE>))
{
    if ($line =~ /^(.+)\t.+\t(.+)$/)
    {
        my $symbol      = $1;
        my $instance_nb = $2;

        $symbols{$symbol} = $instance_nb;
    }
}

close(FILE);

#-------------------------------------------------------------------------------

# Assuming there is one key for each symbol

my $symbol_number = $key_number;


# Sorting symbols by instance number (decreasing)

my @sorted_symbols;
my $i = 0;
my $skipped_digit = 0;

foreach my $key (sort{$symbols{$b} <=> $symbols{$a}} (keys(%symbols)))
{
    $sorted_symbols[$i] = $key;

    $i++;
}

# Getting less frequent symbol being mapped on keymap with direct access 

my $last_symbol = $sorted_symbols[$symbol_number - 1];

#-------------------------------------------------------------------------------

# Getting digraph statistics

my $prev_symbol;

open(FILE, "< $CORPUS_FILE") or die("open: $!");

while (defined(my $line = <FILE>))
{
    for (my $i = 0; $i < length($line); $i++)
    {
        my $symbol = substr($line, $i, 1);

        if (defined($symbols{$symbol}))
            # Known symbol (direct access)
        {
            if (defined($prev_symbol) and ($symbol ne $prev_symbol))
            {
                my $digraph = $prev_symbol."\t".$symbol;
                     
                if (exists($digraphs{$digraph})) { $digraphs{$digraph}++; }
                else { $digraphs{$digraph} = 1; }

                $stored_digraphs_nb++;
            }
            $prev_symbol = $symbol;
        }
        elsif ($symbol =~ /[����������������������������]/)
            # Stressed letter
        {
            my ($stress, $letter) = get_stress_letter($symbol);

            # Must repeat operation for letter and for stress symbol

            if (defined($symbols{$stress}))
                # Known stress (direct access)
            {
                if (defined($prev_symbol) and ($stress ne $prev_symbol))
                {
                    my $digraph = $prev_symbol."\t".$stress;

                    if (exists($digraphs{$digraph})) { $digraphs{$digraph}++; }
                    else { $digraphs{$digraph} = 1; }

                    $stored_digraphs_nb++;
                }
                $prev_symbol = $stress;
            }
            else
                # No direct access to stress
            {
                # Breaking digraph
                undef $prev_symbol;
            }

            if (defined($symbols{$letter}))
                # Known letter (direct access)
            {
                if (defined($prev_symbol) and ($letter ne $prev_symbol))
                {
                    my $digraph = $prev_symbol."\t".$letter;

                    if (exists($digraphs{$digraph})) { $digraphs{$digraph}++; }
                    else { $digraphs{$digraph} = 1; }

                    $stored_digraphs_nb++;
                }
                $prev_symbol = $letter;
            }
            else
                # No direct access to letter
                # Should not happen (keymap without direct acces to some letter, uh ?)
            {
                # Breaking digraph
                undef $prev_symbol;
            }
        }
        else
            # Unknown symbol
        {
            # Breaking digraph
            undef $prev_symbol;
        }
    }
}

close(FILE);

#-------------------------------------------------------------------------------

# Display digraph list with instance number

open(FILE, "> $DIGRAPH_FILE") or die("open: $!");

$i = 1;

foreach my $key (sort{$digraphs{$b} <=> $digraphs{$a}} (keys(%digraphs)))
{
    my $percent = sprintf("%.3f", 100 * $digraphs{$key} / $stored_digraphs_nb);

    print FILE "$key\t$i\t$digraphs{$key}\t$percent\n";

    $i++;
}

close(FILE);
