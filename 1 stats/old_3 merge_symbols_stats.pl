#!/usr/bin/perl -w

#  This file is part of KeyboardGenerator.
#
#  KeyboardGenerator is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  KeyboardGenerator is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with KeyboardGenerator.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use POSIX; # For floor

my @args = [];

my %symbols = (); # Hash table of stored symbols

#-------------------------------------------------------------------------------

die("Usage: $0 <corpus file1> <weighting1> <corpus file2> ... <output file>\n")
    if (($#ARGV % 2) || ($#ARGV == 0));

for (my $i = 0; $i < $#ARGV; $i++)
{
    $args[$i] = $ARGV[$i];
}

my %files = @args;

my $SYMBOL_FILE = $ARGV[$#ARGV];

#-------------------------------------------------------------------------------

foreach my $file (keys %files)
{
    my $weighting = $files{$file};

    open(FILE, "< $file") or die("open: $!");

    while (defined(my $line = <FILE>))
    {
        if ($line =~ /^(.+)\t.+\t(.+)$/)
        {
            my $symbol      = $1;
            my $instance_nb = $2;

            my $weighted_instance_nb = floor($instance_nb * $weighting + 0.5);

            if (exists($symbols{$symbol}))
            {
                $symbols{$symbol} += $weighted_instance_nb;
            }
            else { $symbols{$symbol} = $weighted_instance_nb; }
        }
    }

    close(FILE);
}

#-------------------------------------------------------------------------------

# Store symbol statistics

open(FILE, "> $SYMBOL_FILE") or die("open: $!");

my $i = 1;

foreach my $symbol (sort{$symbols{$b} <=> $symbols{$a}} (keys(%symbols)))
{
    print FILE "$symbol\t$i\t$symbols{$symbol}\n";

    $i++;
}

close(FILE);
