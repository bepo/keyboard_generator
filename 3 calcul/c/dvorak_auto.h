/*
 *  This file is part of KeyboardGenerator.
 *
 *  KeyboardGenerator is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  KeyboardGenerator is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with KeyboardGenerator.  If not, see <http://www.gnu.org/licenses/>.
 */

/*------------------------------------------------------------------------------
 * Nom du projet : Clavier fr-dvorak-b�po, KeyboardGenerator
 * Date de d�but : 21.12.2004
 * Auteur        : Nicolas Chartier
 * Partie        : Programme principal
 * Fichier       : dvorak_auto.h
 *----------------------------------------------------------------------------*/

/* Fichiers contenant les configurations */

#define CONF_COUT_TOUCHES    "cout_categorie_touches.txt"
#define CONF_COUT_SEQUENCES2 "cout_categorie_sequences2.txt"
#define CONF_COUT_SEQUENCES3 "cout_categorie_sequences3.txt"

#define CONF_SYMBOLES        "7_decoupage1.txt"
#define CONF_DIGRAMMES       "8_digrammes.txt"
#define CONF_TRIGRAMMES      "9_trigrammes.txt"

#define CONF_TOUCHES         "infos_touches.txt"
#define CONF_SEQUENCES2      "infos_sequences2.txt"
#define CONF_SEQUENCES3      "infos_sequences3.txt"

#define INIT_SYMBOLES        "init_symboles.txt"
#define INIT_TOUCHES         "init_touches.txt"
#define INIT_MAPPINGS        "init_mappings.txt"

/*----------------------------------------------------------------------------*/

/* Constantes de tailles de champs et de liste */

#define MAX_SIZE_NOM_SYMBOLE    6
#define MAX_SIZE_NOM_TOUCHE     4
#define MAX_CATEGORIE_TOUCHE    6
#define MAX_CATEGORIE_SEQUENCE2 4
#define MAX_CATEGORIE_SEQUENCE3 2

#define MAX_SYMBOL_NUMBER       48
#define MAX_KEY_NUMBER          MAX_SYMBOL_NUMBER

/*----------------------------------------------------------------------------*/

/* Types et structures */

typedef unsigned long Cost;
typedef unsigned int Index;
typedef unsigned int Frequency;


/* Touche */
typedef char KeyName[MAX_SIZE_NOM_TOUCHE + 1];
typedef struct
{
  KeyName name;
  Index class;
  Cost cost;
} Key;

/* Liste de touches */
typedef struct
{
  Key keys[MAX_KEY_NUMBER];
  unsigned int size;
} KeyList;

/* Liste d'index de touches */
typedef struct
{
  Index index[MAX_KEY_NUMBER];
  unsigned int size;
} KeyIndexList;

/* Liste de liste d'index de touches par cat�gorie */
typedef KeyIndexList KeyClassList[MAX_CATEGORIE_TOUCHE];



/* Symbole */
typedef char SymbolName[MAX_SIZE_NOM_SYMBOLE + 1];
typedef struct
{
  SymbolName name;
  Index class;
  Frequency frequency;
} Symbol;

/* Liste de symboles */
typedef struct
{
  Symbol symbols[MAX_SYMBOL_NUMBER];
  unsigned int size;
} SymbolList;

/* Liste d'index de symboles */
typedef struct
{
  Index index[MAX_SYMBOL_NUMBER];
  unsigned int size;
} SymbolIndexList;

/* Liste de liste d'index de symboles par cat�gorie */
typedef SymbolIndexList SymbolClassList[MAX_CATEGORIE_TOUCHE];



/* Association touche/symbole */
typedef struct
{
  Index key;
  Index symbol;
} Mapping;

/* Liste d'association touche/symbole */
typedef struct
{
  Mapping mappings[MAX_KEY_NUMBER];
  unsigned int size;
} MappingList;



/* S�quence de touches */
typedef Cost SequenceCostList[MAX_KEY_NUMBER * MAX_KEY_NUMBER];

/* S�quence de symboles */
typedef Frequency SequenceFrequencyList[MAX_SYMBOL_NUMBER * MAX_SYMBOL_NUMBER];



/*----------------------------------------------------------------------------*/

/* Variables globales */

Cost minCost; /* Co�t minimum */

KeyList keys;                               /* Liste des touches */
KeyClassList keyClasses;                    /* Liste des listes d'index de touches par cat�gorie */
SequenceCostList sequenceCosts2;            /* Liste des co�ts des s�quences de 2 touches */
SequenceCostList sequenceCosts3;            /* Liste des co�ts des s�quences de 3 touches */

SymbolList symbols;                         /* Liste des symboles */
SymbolClassList symbolClasses;              /* Liste des listes d'index de symboles par cat�gorie */
SequenceFrequencyList sequenceFrequencies2; /* Liste des fr�quences des s�quences de digrammes */
SequenceFrequencyList sequenceFrequencies3; /* Liste des fr�quences des s�quences de trigrammes */

KeyIndexList freeKeys;                      /* Liste des index des touches libres */
SymbolIndexList unmappedSymbols;            /* Liste des symboles restant � mapper */
MappingList mappings;                       /* Liste des association touche/symbole */

Cost bestCost;                              /* Co�t du meuilleur mapping trouv� */
MappingList bestMapping;                    /* Meilleur mapping trouv� */

int nbEtapes;

/*----------------------------------------------------------------------------*/

/* Prototypes */

int loadKeys(char *, char *, KeyList *, KeyClassList *);
int loadSequenceCosts2(char *, char *, SequenceCostList *);
int loadSequenceCosts3(char *, char *, SequenceCostList *);

int loadSymbols(char *, SymbolList *, SymbolClassList *, KeyClassList *);
int loadSequenceFrequencies2(char *, SequenceFrequencyList *);
int loadSequenceFrequencies3(char *, SequenceFrequencyList *);

int loadFreeKeys(KeyIndexList *, int);
int loadUnmappedSymbols(SymbolIndexList *, int);
int loadMappings(MappingList *, int);

Cost partialCost(unsigned int, Cost);
Cost localCost(Cost, Index, Index);
int init();
int step(int, int, int);
int copyMapping(MappingList *, MappingList *);
int equalMapping(MappingList *, MappingList *);

Index searchKey(char *);
int affichage();
