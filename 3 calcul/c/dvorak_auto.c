/*
 *  This file is part of KeyboardGenerator.
 *
 *  KeyboardGenerator is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  KeyboardGenerator is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with KeyboardGenerator.  If not, see <http://www.gnu.org/licenses/>.
 */

/*------------------------------------------------------------------------------
 * Nom du projet : Clavier fr-dvorak-b�po, KeyboardGenerator
 * Date de d�but : 21.12.2004
 * Auteur        : Nicolas Chartier
 * Partie        : Programme principal
 * Fichier       : dvorak_auto.c
 *----------------------------------------------------------------------------*/

#include "dvorak_auto.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*----------------------------------------------------------------------------*/

int loadKeys(char *keyFilename, char *keyClassFilename, KeyList *kl,
             KeyClassList *kcl)
{
  FILE *fd;
  Index i = 0;
/*   Index j; */
  int g; /* garbage pour fscanf */
  Index classIndex;

  typedef struct
  {
    Index index;
    Cost cost;
  } KeyClass;

  KeyClass keyClasses[MAX_CATEGORIE_TOUCHE];

  /* Chargement des co�ts des cat�gories de touche */

  if ((fd = fopen(keyClassFilename, "r")) == NULL)
    {
      fprintf(stderr, "Impossible d'ouvrir %s en lecture.\n", keyClassFilename);
      exit(0);
    }

  while (!feof(fd))
    {
      if (fscanf(fd, "%u\t%lu", &keyClasses[i].index, &keyClasses[i].cost) == 2)
        {
/*           printf("%u\t%lu\n", keyClasses[i].index, keyClasses[i].cost); */
          i++;
        }
    }

  fclose(fd);

  /* Chargement des touches */

  i = 0;

  if ((fd = fopen(keyFilename, "r")) == NULL)
    {
      fprintf(stderr, "Impossible d'ouvrir %s en lecture.\n", keyFilename);
      exit(0);
    }

  while (!feof(fd))
    {
      if (fscanf(fd, "%s\t%u\t%d\t%d\t%d", kl->keys[i].name, &classIndex,
                 &g, &g, &g) == 5)
        {
          kl->keys[i].class = classIndex - 1;
          kl->keys[i].cost  = keyClasses[classIndex - 1].cost;

          (*kcl)[classIndex - 1].index[(*kcl)[classIndex - 1].size] = i;
          (*kcl)[classIndex - 1].size++;

/*           printf("%u\t%s\t%u\t%lu\n", i, kl->keys[i].name, kl->keys[i].class, */
/*                  kl->keys[i].cost); */
          i++;
        }
    }

  fclose(fd);

  kl->size = i;

/*   for (i = 0; i < MAX_CATEGORIE_TOUCHE; i++) */
/*     for (j = 0; j < (*kcl)[i].size; j++) */
/*       printf("%u\t%u\t%s\n", i, j, kl->keys[(*kcl)[i].index[j]].name); */

  return 0;
}

/*----------------------------------------------------------------------------*/

int loadSequenceCosts2(char *sequenceFilename, char *sequenceCostFilename,
                       SequenceCostList *scl)
{
  FILE *fd;
  Index i = 0;
  Index costIndex;
  Index key1Index = 0;
  Index key2Index = 0;
  KeyName key1, key2;
  int foundKey1, foundKey2;

  typedef struct
  {
    Index index;
    Cost cost;
  } SequenceClass;

  SequenceClass sequenceClasses[MAX_CATEGORIE_SEQUENCE2];

  /*
   * Initialisation inutile car on ne lira jamais le cases non initilalis�es
   *
   * for (i = 0; i < MAX_KEY_NUMBER * MAX_KEY_NUMBER; i++)
   *   (*scl)[i] = 0;
   *
   * i = 0;
   */

  /* Chargement des co�ts des cat�gories des s�quences de 2 touches */

  if ((fd = fopen(sequenceCostFilename, "r")) == NULL)
    {
      fprintf(stderr, "Impossible d'ouvrir %s en lecture.\n",
              sequenceCostFilename);
      exit(0);
    }

  while (!feof(fd))
    {
      if (fscanf(fd, "%u\t%lu", &sequenceClasses[i].index,
                 &sequenceClasses[i].cost) == 2)
        {
/*           printf("%u\t%lu\n", sequenceClasses[i].index, */
/*                  sequenceClasses[i].cost); */
          i++;
        }
    }

  fclose(fd);

  /* Chargement des s�quences de 2 touches */

  if ((fd = fopen(sequenceFilename, "r")) == NULL)
    {
      fprintf(stderr, "Impossible d'ouvrir %s en lecture.\n", sequenceFilename);
      exit(0);
    }

  while (!feof(fd))
    {
      foundKey1 = 0;
      foundKey2 = 0;

      if (fscanf(fd, "%s - %s\t%u", key1, key2, &costIndex) == 3)
        {
          /* On cherche les index des touches correspondantes aux noms lus */
          for (i = 0; i < keys.size; i++)
            {
              if (! strncmp(keys.keys[i].name, key1, MAX_SIZE_NOM_TOUCHE))
                {
                  foundKey1 = 1;
                  key1Index = i;
                }
              if (! strncmp(keys.keys[i].name, key2, MAX_SIZE_NOM_TOUCHE))
                {
                  foundKey2 = 1;
                  key2Index = i;
                }
            }

          if (! foundKey1)
            printf("Touche %s non trouv�e\n", key1);

          if (! foundKey2)
            printf("Touche %s non trouv�e\n", key2);

          if (foundKey1 && foundKey2)
            {
              (*scl)[key1Index * MAX_KEY_NUMBER + key2Index] =
                sequenceClasses[costIndex - 1].cost;
/*               printf("%u\t%s - %s\t%lu\n", */
/*                      key1Index * MAX_KEY_NUMBER + key2Index, */
/*                      key1, key2, sequenceClasses[costIndex - 1].cost); */
            }
        }
    }

  fclose(fd);

/*   for (i = 0; i < MAX_KEY_NUMBER * MAX_KEY_NUMBER; i++) */
/*     printf("%lu\n", (*scl)[i]); */

  return 0;
}

/*----------------------------------------------------------------------------*/

int loadSequenceCosts3(char *sequenceFilename, char *sequenceCostFilename,
                       SequenceCostList *scl)
{
  FILE *fd;
  Index i = 0;
  Index costIndex;
  Index key1Index = 0;
  Index key2Index = 0;
  KeyName key1, key2;
  int foundKey1, foundKey2;

  typedef struct
  {
    Index index;
    Cost cost;
  } SequenceClass;

  SequenceClass sequenceClasses[MAX_CATEGORIE_SEQUENCE3];

  /*
   * Initialisation inutile car on ne lira jamais le cases non initilalis�es
   *
   * for (i = 0; i < MAX_KEY_NUMBER * MAX_KEY_NUMBER; i++)
   *   (*scl)[i] = 0;
   *
   * i = 0;
   */

  /* Chargement des co�ts des cat�gories des s�quences de 3 touches */

  if ((fd = fopen(sequenceCostFilename, "r")) == NULL)
    {
      fprintf(stderr, "Impossible d'ouvrir %s en lecture.\n",
              sequenceCostFilename);
      exit(0);
    }

  while (!feof(fd))
    {
      if (fscanf(fd, "%u\t%lu", &sequenceClasses[i].index,
                 &sequenceClasses[i].cost) == 2)
        {
/*           printf("%u\t%lu\n", sequenceClasses[i].index, */
/*                  sequenceClasses[i].cost); */
          i++;
        }
    }

  fclose(fd);

  /* Chargement des s�quences de 3 touches */

  if ((fd = fopen(sequenceFilename, "r")) == NULL)
    {
      fprintf(stderr, "Impossible d'ouvrir %s en lecture.\n", sequenceFilename);
      exit(0);
    }

  while (!feof(fd))
    {
      foundKey1 = 0;
      foundKey2 = 0;

      if (fscanf(fd, "%s - %s\t%u", key1, key2, &costIndex) == 3)
        {
          /* On cherche les index des touches correspondantes aux noms lus */
          for (i = 0; i < keys.size; i++)
            {
              if (! strncmp(keys.keys[i].name, key1, MAX_SIZE_NOM_TOUCHE))
                {
                  foundKey1 = 1;
                  key1Index = i;
                }
              if (! strncmp(keys.keys[i].name, key2, MAX_SIZE_NOM_TOUCHE))
                {
                  foundKey2 = 1;
                  key2Index = i;
                }
            }

          if (! foundKey1)
            printf("Touche %s non trouv�e\n", key1);

          if (! foundKey2)
            printf("Touche %s non trouv�e\n", key2);

          if (foundKey1 && foundKey2)
            {
              (*scl)[key1Index * MAX_KEY_NUMBER + key2Index] =
                sequenceClasses[costIndex - 1].cost;
/*               printf("%u\t%s - %s\t%lu\n", */
/*                      key1Index * MAX_KEY_NUMBER + key2Index, */
/*                      key1, key2, sequenceClasses[costIndex - 1].cost); */
            }
        }
    }

  fclose(fd);

/*   for (i = 0; i < MAX_KEY_NUMBER * MAX_KEY_NUMBER; i++) */
/*     printf("%lu\n", (*scl)[i]); */

  return 0;
}

/*----------------------------------------------------------------------------*/

int loadSymbols(char *filename, SymbolList *sl, SymbolClassList *scl,
                KeyClassList *kcl)
{
  FILE *fd;
  Index i = 0;
/*   Index j; */
  Index class = 0;
  Index indexInClass = 0;
  int g; /* garbage pour fscanf */

  if ((fd = fopen(filename, "r")) == NULL)
    {
      fprintf(stderr, "Impossible d'ouvrir %s en lecture.\n", filename);
      exit(0);
    }

  while (!feof(fd))
    {
      if (fscanf(fd, "%s\t%d\t%u", sl->symbols[i].name, &g,
                 &sl->symbols[i].frequency) == 3)
        {
          sl->symbols[i].class = class;

          (*scl)[class].index[indexInClass] = i;

          indexInClass++;
          (*scl)[class].size++;

          if (indexInClass >= (*kcl)[class].size)
            {
              class++;
              indexInClass = 0;
            }

/*           printf("%u\t%s\t%u\t%u\n", i, sl->symbols[i].name, */
/*                  sl->symbols[i].class, sl->symbols[i].frequency); */
          i++;
        }
    }

  fclose(fd);

  sl->size = i;

/*   for (i = 0; i < MAX_CATEGORIE_TOUCHE; i++) */
/*     for (j = 0; j < (*scl)[i].size; j++) */
/*       printf("%u\t%u\t%s\n", i, j, sl->symbols[(*scl)[i].index[j]].name); */

  return 0;
}

/*----------------------------------------------------------------------------*/

int loadSequenceFrequencies2(char *filename, SequenceFrequencyList *sfl)
{
  FILE *fd;
  Index i;
  int g_i; /* garbage pour fscanf */
  float g_f; /* garbage pour fscanf */
  Frequency frequency; /* Frequence de la s�quence lue */
  Index symbol1Index = 0;
  Index symbol2Index = 0;
  SymbolName symbol1, symbol2;
  int foundSymbol1, foundSymbol2;

  if ((fd = fopen(filename, "r")) == NULL)
    {
      fprintf(stderr, "Impossible d'ouvrir %s en lecture.\n", filename);
      exit(0);
    }

  while (!feof(fd))
    {
      foundSymbol1 = 0;
      foundSymbol2 = 0;

      if (fscanf(fd, "%s\t%s\t%d\t%u\t%f", symbol1, symbol2, &g_i,
                 &frequency, &g_f) == 5)
        {
          /* On cherche les index des symboles correspondants aux noms lus */
          for (i = 0; i < symbols.size; i++)
            {
              if (! strncmp(symbols.symbols[i].name, symbol1,
                            MAX_SIZE_NOM_SYMBOLE))
                {
                  foundSymbol1 = 1;
                  symbol1Index = i;
                }

              if (! strncmp(symbols.symbols[i].name, symbol2,
                            MAX_SIZE_NOM_SYMBOLE))
                {
                  foundSymbol2 = 1;
                  symbol2Index = i;
                }
            }

          if (! foundSymbol1)
            printf("Symbole %s non trouv�\n", symbol1);

          if (! foundSymbol2)
            printf("Symbole %s non trouv�\n", symbol2);

          if (foundSymbol1 && foundSymbol2)
            {
              (*sfl)[symbol1Index * MAX_SYMBOL_NUMBER + symbol2Index] =
                frequency;
/*               printf("%u\t%s - %s\t%u\n", */
/*                      symbol1Index * MAX_SYMBOL_NUMBER + symbol2Index, */
/*                      symbol1, symbol2, frequency); */
            }
        }
    }

  fclose(fd);

/*   for (i = 0; i < MAX_SYMBOL_NUMBER * MAX_SYMBOL_NUMBER; i++) */
/*     printf("%u\n", (*sfl)[i]); */

  return 0;
}

/*----------------------------------------------------------------------------*/

int loadSequenceFrequencies3(char *filename, SequenceFrequencyList *sfl)
{
  FILE *fd;
  Index i;
  int g_i; /* garbage pour fscanf */
  float g_f; /* garbage pour fscanf */
  Frequency frequency; /* Frequence de la s�quence lue */
  Index symbol1Index = 0;
  Index symbol2Index = 0;
  SymbolName symbol1, symbol2;
  int foundSymbol1, foundSymbol2;

  if ((fd = fopen(filename, "r")) == NULL)
    {
      fprintf(stderr, "Impossible d'ouvrir %s en lecture.\n", filename);
      exit(0);
    }

  while (!feof(fd))
    {
      foundSymbol1 = 0;
      foundSymbol2 = 0;

      if (fscanf(fd, "%s\t%s\t%d\t%u\t%f", symbol1, symbol2, &g_i,
                 &frequency, &g_f) == 5)
        {
          /* On cherche les index des symboles correspondants aux noms lus */
          for (i = 0; i < symbols.size; i++)
            {
              if (! strncmp(symbols.symbols[i].name, symbol1,
                            MAX_SIZE_NOM_SYMBOLE))
                {
                  foundSymbol1 = 1;
                  symbol1Index = i;
                }

              if (! strncmp(symbols.symbols[i].name, symbol2,
                            MAX_SIZE_NOM_SYMBOLE))
                {
                  foundSymbol2 = 1;
                  symbol2Index = i;
                }
            }

          if (! foundSymbol1)
            printf("Symbole %s non trouv�\n", symbol1);

          if (! foundSymbol2)
            printf("Symbole %s non trouv�\n", symbol2);

          if (foundSymbol1 && foundSymbol2)
            {
              (*sfl)[symbol1Index * MAX_SYMBOL_NUMBER + symbol2Index] =
                frequency;
/*               printf("%u\t%s - %s\t%u\n", */
/*                      symbol1Index * MAX_SYMBOL_NUMBER + symbol2Index, */
/*                      symbol1, symbol2, frequency); */
            }
        }
    }

  fclose(fd);

/*   for (i = 0; i < MAX_SYMBOL_NUMBER * MAX_SYMBOL_NUMBER; i++) */
/*     printf("%u\n", (*sfl)[i]); */

  return 0;
}

/*----------------------------------------------------------------------------*/

int loadFreeKeys(KeyIndexList *kil, int classInCycle)
{
  Index i;

  for (i = 0; i < keyClasses[classInCycle].size; i++)
    kil->index[i] = keyClasses[classInCycle].index[i];

  kil->size = keyClasses[classInCycle].size;
    
/*   for (i = 0; i < kil->size; i++) */
/*     printf("%u\t%s\n", i, keys.keys[kil->index[i]].name); */
  printf("%u touches", kil->size);

  return 0;
}

/*----------------------------------------------------------------------------*/

int loadUnmappedSymbols(SymbolIndexList *sil, int classInCycle)
{
  Index i;

  for (i = 0; i < symbolClasses[classInCycle].size; i++)
    sil->index[i] = symbolClasses[classInCycle].index[i];

  sil->size = symbolClasses[classInCycle].size;

/*   for (i = 0; i < sil->size; i++) */
/*     printf("%u\t%s\n", i, symbols.symbols[sil->index[i]].name); */
  printf(" sur lesquelles on doit mapper %u symboles\n", sil->size);

  return 0;
}

/*----------------------------------------------------------------------------*/

int loadMappings(MappingList *ml, int classInCycle)
{
  Index i = 0;
  Index j;

/*   printf("avant : ml->size : %u\n", ml->size); */

/*   for (i = 0; i < ml->size; i++) */
/*     printf("%u\t%s\t%s\n", i, */
/*            keys.keys[ml->mappings[i].key].name, */
/*            symbols.symbols[ml->mappings[i].symbol].name); */

/*   printf("classe � virer : %d\n", classInCycle); */

  i = 0;

  while (i < ml->size)
    {
/*       printf("classe de %s : %u\n", */
/*              symbols.symbols[ml->mappings[i].symbol].name, */
/*              symbols.symbols[ml->mappings[i].symbol].class); */
      if (symbols.symbols[ml->mappings[i].symbol].class == classInCycle)
        {
          /* Pour supprimer une valeur */

          ml->size--;

          /* On d�cale d'un cran toute celle au del� */
          for (j = i; j < ml->size; j++)
            {
              ml->mappings[j].key    = ml->mappings[j + 1].key;
              ml->mappings[j].symbol = ml->mappings[j + 1].symbol;
            }
        }
      else
        i++;
    }

/*   printf("apres : ml->size : %u\n\n", ml->size); */

/*   for (i = 0; i < ml->size; i++) */
/*     printf("%u\t%s\t%s\n", i, */
/*            keys.keys[ml->mappings[i].key].name, */
/*            symbols.symbols[ml->mappings[i].symbol].name); */

  return 0;
}

/*----------------------------------------------------------------------------*/

/* Fonction calculant le co�t partiel et faisant des appels r�cursifs sur
 * chaque combinaison */

Cost partialCost(unsigned int depth, /* Profondeur dans l'arbre de recherche */
                 Cost cost) /* Co�t courant */
{
  Index symbol; /* Index du symbole */
  Index key; /* Index de la touche */
  Index i, k;

  Cost oldCost = cost; /* Sauvegarde du co�t courant */

  if (depth == unmappedSymbols.size + 1)
    /* Feuille de l'arbre de recherche */
    return cost;

  /* Noeud de l'arbre de recherche */

  /*
   * On r�cup�re le symbole le plus courant restant � mapper
   * La liste �tant tri�e par ordre d�croissant du nombre d'occurences, 
   * c'est donc l'�l�ment de la liste dont l'index est la profondeur
   */
  symbol = unmappedSymbols.index[depth - 1];

  for (i = 0; i < freeKeys.size; i++)
    /* Pour chaque touche libre */
    {
      /* On restaure le co�t */
      cost = oldCost;

      /* On r�cup�re la touche */
      key = freeKeys.index[i];

      /* On la retire des touches libres */
      /* On r�duit la taille de la liste */
      freeKeys.size--;
      /* Puis on fait �a via interversion des valeurs dans la liste */
      freeKeys.index[i] = freeKeys.index[freeKeys.size];

      /*
       * for (k = i; k < freeKeys.size; k++)
       * freeKeys.index[k] = freeKeys.index[k + 1];
       */

      /*
       * Inutile
       *
       * freeKeys.index[freeKeys.size] = key;
       */
      cost = localCost(cost, key, symbol);

      if (cost < minCost)
        /* Si le co�t courant est toujours inf�rieur au co�t minimum */
        {
          /* Alors on effectue l'appel r�cursif */

          /*
           * On stocke la touche et le symbole dans la liste des associations
           * d�ja r�alis�es
           */
          mappings.mappings[mappings.size].key    = key;
          mappings.mappings[mappings.size].symbol = symbol;
          mappings.size++;

          /* Appel r�cursif */
          cost = partialCost(depth + 1, cost);

          if (cost < minCost)
            /* Nouveau co�t minimal */
            {
              /* M�moriser le mapping actuel */
              for (k = 0; k < mappings.size; k++)
                {
                  /* On le stocke en m�moire */
                  bestMapping.mappings[k].key    = mappings.mappings[k].key;
                  bestMapping.mappings[k].symbol = mappings.mappings[k].symbol;
              
                  /* On le stock sur disque */
                  /* TODO */

                  /* Et on l'affiche */
/*                   printf("%s %s\t", */
/*                          keys.keys[mappings.mappings[k].key].name, */
/*                          symbols.symbols[mappings.mappings[k].symbol].name); */
                }
              nbEtapes++;

              bestCost = cost;
              bestMapping.size = mappings.size;

              minCost = cost;
              
/*               printf("Nouveau co�t min : %lu\n", minCost); */
            }

          /* Supprimer le mapping stocker avant l'appel r�cursif */
          mappings.size--;
        }
      /* Sinon on ne fait rien */

      /* Remettre la touche dans la liste des touches libres */

      /*
       * for (k = freeKeys.size; k > i; k--)
       * freeKeys.index[k] = freeKeys.index[k - 1];
       */

      freeKeys.index[freeKeys.size] = freeKeys.index[i];
      freeKeys.index[i] = key;
      freeKeys.size++;
    }

  return cost;
}

/*----------------------------------------------------------------------------*/

Cost localCost(Cost cost, Index key, Index symbol)
{
  Index j;

  Index mappedKey; /* Index de la touche mapp�e */
  Index mappedSymbol; /* Index du symbole mapp� */
  Index sequence; /* Index de la s�quence de touches */

  Cost sequenceCost2, sequenceCost3; /* Co�ts de s�quence de touches */

  Frequency sequenceFrequency2, sequenceFrequency3; /* Fr�quences des s�quences de digrammes et trigrammes */

  for (j = 0; j < mappings.size; j++)
    /* Pour chaque association touche/symbole d�ja effectu�e */
    {
      /* On r�cup�re la touche et le symbole */
      mappedKey    = mappings.mappings[j].key;
      mappedSymbol = mappings.mappings[j].symbol;


      /* On cherche le co�t de la s�quence des deux touches */


      /* S�quence ancienne - nouvelle */

      /* Co�t de la s�quence de touches */
      sequence = key * MAX_KEY_NUMBER + mappedKey;
      sequenceCost2 = sequenceCosts2[sequence];
      sequenceCost3 = sequenceCosts3[sequence];

      /* Nombre d'occurences de la s�quence de symboles */
      sequence = symbol * MAX_SYMBOL_NUMBER + mappedSymbol;
      sequenceFrequency2 = sequenceFrequencies2[sequence];
      sequenceFrequency3 = sequenceFrequencies3[sequence];

      /* Augmentation du co�t */
      cost += (sequenceFrequency2 * sequenceCost2);
      cost += (sequenceFrequency3 * sequenceCost3);


      /* S�quence nouvelle - ancienne */

      /* Co�t de la s�quence de touches */
      sequence = mappedKey * MAX_KEY_NUMBER + key;
      sequenceCost2 = sequenceCosts2[sequence];
      sequenceCost3 = sequenceCosts3[sequence];

      /* Nombre d'occurences de la s�quence de symboles */
      sequence = mappedSymbol * MAX_SYMBOL_NUMBER + symbol;
      sequenceFrequency2 = sequenceFrequencies2[sequence];
      sequenceFrequency3 = sequenceFrequencies3[sequence];

      /* Augmentation du co�t */
      cost += (sequenceFrequency2 * sequenceCost2);
      cost += (sequenceFrequency3 * sequenceCost3);
    }

  return cost;
}

/*----------------------------------------------------------------------------*/

int copyMapping(MappingList *source , MappingList *target)
{
  Index i;

  for (i = 0; i < source->size; i++)
    {
      target->mappings[i].key    = source->mappings[i].key;
      target->mappings[i].symbol = source->mappings[i].symbol;
    }

  target->size = source->size;

  return 0;
}

/*----------------------------------------------------------------------------*/

int equalMapping(MappingList *map1 , MappingList *map2)
{
  Index i, j;
  int equal1 = 1; /* Bool�en pour tout le mapping */
  int equal2;     /* Bool�en pour une association */

  if (map1->size != map2->size)
    {
      printf("PAS EGAL (taille diff : %u avant, %u apr�s)\n\n", map1->size, map2->size);

      return 0;
    }

  for (i = 0; i < map1->size; i++)
    {
      equal2 = 0;

      for (j = 0; j < map2->size; j++)
        if ((map1->mappings[i].key    == map2->mappings[j].key) &&
            (map1->mappings[i].symbol == map2->mappings[j].symbol))
          equal2 = 1;

      equal1 = equal1 && equal2;
    }

  if (equal1)
    printf("EGAL\n\n");
  else
    printf("PAS EGAL\n\n");

  return equal1;
}

/*----------------------------------------------------------------------------*/

int init()
{
  loadKeys(CONF_TOUCHES, CONF_COUT_TOUCHES, &keys, &keyClasses);
  loadSequenceCosts2(CONF_SEQUENCES2, CONF_COUT_SEQUENCES2, &sequenceCosts2);
  loadSequenceCosts3(CONF_SEQUENCES3, CONF_COUT_SEQUENCES3, &sequenceCosts3);

  loadSymbols(CONF_SYMBOLES, &symbols, &symbolClasses, &keyClasses);
  loadSequenceFrequencies2(CONF_DIGRAMMES, &sequenceFrequencies2);
  loadSequenceFrequencies3(CONF_TRIGRAMMES, &sequenceFrequencies3);

  return 0;
}

/*----------------------------------------------------------------------------*/

int step(int class, int classInCycle, int cycle)
{
  Index i;
  MappingList avant;
  copyMapping(&bestMapping, &mappings);
  copyMapping(&bestMapping, &avant);

  printf("*************************************\n");
  printf("*************** %d_%d_%d ***************\n", class, classInCycle, cycle);
  printf("*************************************\n");

  minCost = (unsigned int)-1;

  loadFreeKeys(&freeKeys, classInCycle - 1);
  loadUnmappedSymbols(&unmappedSymbols, classInCycle - 1);
  loadMappings(&mappings, classInCycle - 1);

  nbEtapes = 0;

  partialCost(1, 0);

  printf("Nombre d'�tapes : %d\n", nbEtapes);
  for (i = 0; i < bestMapping.size; i++)
    printf("%s %s\t", keys.keys[bestMapping.mappings[i].key].name,
           symbols.symbols[bestMapping.mappings[i].symbol].name);
  printf("Nouveau co�t min : %lu\n", bestCost);

  return equalMapping(&avant, &bestMapping);
}

/*----------------------------------------------------------------------------*/

Index searchKey(char *name)
{
  Index i;

  for (i = 0; i < keys.size; i++)
    if (strncmp(keys.keys[bestMapping.mappings[i].key].name, name, 4) == 0)
      return i;

  return 0;
}

/*----------------------------------------------------------------------------*/

int affichage()
{
  Index i;
  Index key;
  char keyName[4];

  /* Ligne E */
  key = searchKey("TLDE");
  printf("%s\t", symbols.symbols[bestMapping.mappings[key].symbol].name);
  
  strcpy(keyName, "AE00");
  for(i = 1; i <= 9; i++)
    {
      keyName[3] = '0' + i;
      key = searchKey(keyName);
      printf("%s\t", symbols.symbols[bestMapping.mappings[key].symbol].name);
    }

  key = searchKey("AE10");
  printf("%s\t", symbols.symbols[bestMapping.mappings[key].symbol].name);
  key = searchKey("AE11");
  printf("%s\t", symbols.symbols[bestMapping.mappings[key].symbol].name);
  key = searchKey("AE12");
  printf("%s\t", symbols.symbols[bestMapping.mappings[key].symbol].name);
  printf("\n");

  /* Ligne D */
  printf("\t");

  strcpy(keyName, "AD00");
  for(i = 1; i <= 9; i++)
    {
      keyName[3] = '0' + i;
      key = searchKey(keyName);
      printf("%s\t", symbols.symbols[bestMapping.mappings[key].symbol].name);
    }

  key = searchKey("AD10");
  printf("%s\t", symbols.symbols[bestMapping.mappings[key].symbol].name);
  key = searchKey("AD11");
  printf("%s\t", symbols.symbols[bestMapping.mappings[key].symbol].name);
  key = searchKey("AD12");
  printf("%s\t", symbols.symbols[bestMapping.mappings[key].symbol].name);
  printf("\n");

  /* Ligne C */
  printf("\t");

  strcpy(keyName, "AC00");
  for(i = 1; i <= 9; i++)
    {
      keyName[3] = '0' + i;
      key = searchKey(keyName);
      printf("%s\t", symbols.symbols[bestMapping.mappings[key].symbol].name);
    }

  key = searchKey("AC10");
  printf("%s\t", symbols.symbols[bestMapping.mappings[key].symbol].name);
  key = searchKey("AC11");
  printf("%s\t", symbols.symbols[bestMapping.mappings[key].symbol].name);
  key = searchKey("BKSL");
  printf("%s\t", symbols.symbols[bestMapping.mappings[key].symbol].name);
  printf("\n");

  /* Ligne B */
  key = searchKey("LSGT");
  printf("%s\t", symbols.symbols[bestMapping.mappings[key].symbol].name);

  strcpy(keyName, "AB00");
  for(i = 1; i <= 9; i++)
    {
      keyName[3] = '0' + i;
      key = searchKey(keyName);
      printf("%s\t", symbols.symbols[bestMapping.mappings[key].symbol].name);
    }

  key = searchKey("AB10");
  printf("%s\t", symbols.symbols[bestMapping.mappings[key].symbol].name);
  printf("\n");

  return 0;
}

/*----------------------------------------------------------------------------*/

int main()
{
  Index class; /* Cat�gorie de touche en cours de rajout */
  Index classInCycle; /* Cat�gorie de touche en cours de calcul */
  Index cycle = 0; /* Num�ro du cycle de convergence en cours de calcul */

  int convergence;

  init();

  /* 1er cas � part, mapping pr�c�dent nul */
  step(1, 1, cycle);

  for (class = 2; class <= MAX_CATEGORIE_TOUCHE; class++)
    /* Pour chaque cat�gorie de touche */
    {
      cycle = 0;
      classInCycle = class;

      /* Calcul de la cat�gorie qu'on rajoute */
      step(class, classInCycle, cycle);

      do
        {
          convergence = 1;
          cycle++;

          for(classInCycle = 1; classInCycle <= class; classInCycle++)
            if (! step(class, classInCycle, cycle))
              convergence = 0;
        }
      while (! convergence);
    }

  affichage();

  return 0;
}
