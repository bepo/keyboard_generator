#!/usr/bin/perl -w

#  This file is part of KeyboardGenerator.
#
#  KeyboardGenerator is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  KeyboardGenerator is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with KeyboardGenerator.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use FileHandle;

# Autoflush to keep order in log files.
STDOUT->autoflush;
STDERR->autoflush;

my %keys = ();

#-------------------------------------------------------------------------------

die("Usage: $0 <key infos file> <sequences infos file>\n")
    if (!defined($ARGV[1]));

die("File $ARGV[0] doesn't exist\n")
    if (! -e $ARGV[0]);

my $KEY_FILE       = $ARGV[0];
my $SEQUENCE_FILE  = $ARGV[1];

#-------------------------------------------------------------------------------

# Getting key informations from input file

open(FILE, "< $KEY_FILE") or die("open: $!");

while (defined(my $line = <FILE>))
{
    if ($line =~ /^(....)\t.\t(.)\t(.)\t(.)$/)
        # Data line
    {
        my $name   = $1;
        my $finger = $2;
        my $hand   = $3;

        $keys{$name} = [ $finger, $hand ];
    }
}

close(FILE);

#-------------------------------------------------------------------------------

# Storing key sequence informations to output file

open(FILE, "> $SEQUENCE_FILE") or die("open: $!");

foreach my $name1 (sort keys %keys)
{
    my $finger1 = $keys{$name1}[0];
    my $hand1   = $keys{$name1}[1];

    foreach my $name2 (sort keys %keys)
    {
        next if ($name2 eq $name1);

        my $finger2 = $keys{$name2}[0];
        my $hand2   = $keys{$name2}[1];

        my $sequence_cost_type;

        if ($hand1 == $hand2)
            # Same hand
        {
           $sequence_cost_type = 1;
        }
        else
            # Different hands
        {
            $sequence_cost_type = 2;
        }

        print FILE "$name1 - $name2\t$sequence_cost_type\n";
    }
}

close(FILE);
